// 1. 启动一个后端服务
const express = require("express");
const Exapp = express();
const cors = require("cors");
const bodyParser = require("body-parser");
// 1.2 配置post请求
Exapp.use(bodyParser.urlencoded({ extended: true }));
Exapp.use(bodyParser.json());
// 1.3 配置跨域设置
Exapp.use(cors());

// 设置路由
Exapp.use('/home', require('./route/backendwave.js'))
// 赞港写的后端
Exapp.use('/home', require('./route/backendport.js'))
// 王明月的后端
Exapp.use('/home', require('./route/onthebackend.js'))
// 吴楠的后端
Exapp.use('/home', require('./route/backendwave.js'))

// 1.4 开启后端服务端口
Exapp.listen(7890, () => console.log("http://localhost:7890"));